UPDATE wp_options SET option_value = replace(option_value, 'https://www.foo.com', 'http://127.0.0.1') WHERE option_name = 'home' OR option_name = 'siteurl';
UPDATE wp_posts SET guid = replace(guid, 'https://www.foo.com','http://127.0.0.1');
UPDATE wp_posts SET post_content = replace(post_content, 'https://www.foo.com', 'http://127.0.0.1');
UPDATE wp_postmeta SET meta_value = replace(meta_value, 'https://www.foo.com', 'http://127.0.0.1');
