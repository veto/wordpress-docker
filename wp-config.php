<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wpsql1' );

/** MySQL database username */
define( 'DB_USER', 'wpsql1' );

/** MySQL database password */
define( 'DB_PASSWORD', 'passpass' );

/** MySQL hostname */
define( 'DB_HOST', 'wp_mariadb' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'kxVN;ktIfWSwuG2[=`{0A6(cN;p?H&K8p#7_X>[=_r+h/y|9Nn<O5B~(H(E.rPE{' );
define( 'SECURE_AUTH_KEY',  '>~?8S!Yv%PvTyfn1|T_`naNY}jdW@3(s:;.#YOsG;ve,F3kIe}<7uvR_%l{-0 BX' );
define( 'LOGGED_IN_KEY',    'o8dhS%fkzIf=03<jBU3Nns8j~Z`7qq;US}o#5Rte<}*PTNyk mNs)TWOeMNR8JWn' );
define( 'NONCE_KEY',        ')[_ASAPZ98a0#yq#(FnM2EnlRsPc))5T8w^q9l}?|5iHIw8(*n&=Pif[`rG^&ARJ' );
define( 'AUTH_SALT',        '^-$*XO?kt_wt[8YPzHX`3H^I?Ivfe>l&7vH9UWKC$+P-qh{[n<5F@Z/x+^Vkl}FR' );
define( 'SECURE_AUTH_SALT', 'iQSe7_g#:]Ggp4oz){ C&&;g)(!NVM!#oNzA<?8{/C_I{k]e|?m #2L,kVII<[K ' );
define( 'LOGGED_IN_SALT',   'r59ub5#3@~h~`UyVNSGO!lGvWxl?`GgMrqpt_2a}E$g]WYY/Wb>KKnZ|A5f`T@C8' );
define( 'NONCE_SALT',       'GRjO J;is8N(>l0NQlR<r65,G}(2yO_ZG.?-uiRkWA(nFLJ[vqF2<v$}N)thxxph' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );

